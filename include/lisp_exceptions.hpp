#ifndef LISP_EXCEPTIONS_HPP_
#define LISP_EXCEPTIONS_HPP_

#include <lisp_types.hpp>
#include <stdexcept>

namespace lisp_exceptions
{
    class lisp_exception : public std::runtime_error {
        protected:
            std::string preface;

        public:
            explicit lisp_exception (const char* preface, const std::string& what_arg)
                : runtime_error(what_arg), preface(preface) {};
            explicit lisp_exception (const std::string preface, const std::string& what_arg)
                : runtime_error(what_arg), preface(preface) {};
            explicit lisp_exception (const char* preface, const char* what_arg)
                : runtime_error(what_arg), preface(preface) {};
            explicit lisp_exception (const std::string preface, const char* what_arg)
                : runtime_error(what_arg), preface(preface) {};
            virtual std::string msg(void) const noexcept;
    };

    class illegal_operation : public lisp_exception {
        public:
            explicit illegal_operation (const std::string& what_arg)
                : lisp_exception("Illegal operation: ", what_arg) {};
            explicit illegal_operation (const char* what_arg)
                : lisp_exception("Illegal operation: ", what_arg) {};
    };

    class invalid_argument : public lisp_exception {
        public:
            explicit invalid_argument (const std::string& what_arg)
                : lisp_exception("Invalid argument: ", what_arg) {};
            explicit invalid_argument (const char* what_arg)
                : lisp_exception("Invalid argument: ", what_arg) {};
    };

    class invalid_variable : public lisp_exception {
        public:
            explicit invalid_variable (const std::string& what_arg)
                : lisp_exception("No such variable: ", what_arg) {};
            explicit invalid_variable (const char* what_arg)
                : lisp_exception("No such variable: ", what_arg) {};
            explicit invalid_variable (lisp_types::any what_arg)
                : lisp_exception("No such variable: ", what_arg->get_repr()) {};
    };

    class invalid_function : public lisp_exception {
        public:
            explicit invalid_function (const std::string& what_arg)
                : lisp_exception("No such function: ", what_arg) {};
            explicit invalid_function (const char* what_arg)
                : lisp_exception("No such function: ", what_arg) {};
            explicit invalid_function (lisp_types::any what_arg)
                : lisp_exception("No such function: ", what_arg->get_repr()) {};
    };

    class invalid_call : public lisp_exception {
        public:
            explicit invalid_call (const std::string& what_arg)
                : lisp_exception("Incorrect number of function arguments. Need: ", what_arg) {};
            explicit invalid_call (const char* what_arg)
                : lisp_exception("Incorrect number of function arguments. Need: ", what_arg) {};
            explicit invalid_call (std::string func, int what_arg)
                : lisp_exception("Incorrect number of function arguments. '" + func + "' needs: ", std::to_string(what_arg)) {};
    };

    class parsing_error : public lisp_exception {
        public:
            explicit parsing_error (const std::string& what_arg)
                : lisp_exception("Unable to parse expression: ", what_arg) {};
            explicit parsing_error (const char* what_arg)
                : lisp_exception("Unable to parse expression: ", what_arg) {};
    };
}

#endif