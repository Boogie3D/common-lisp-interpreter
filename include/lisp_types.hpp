#ifndef LISPTYPES_HPP_
#define LISPTYPES_HPP_

#include <list>
#include <string>

// Lisp primitive types
namespace lisp_prims
{
    enum BOOL {NIL, T};
    typedef long INTEGER32;
    typedef double FLOAT64;
    typedef std::string STRING;
}

// Abstract Lisp types
namespace lisp_types
{
    // Abstract base class for atoms and lists
    typedef 
    class _any {
        protected:
            lisp_prims::STRING repr;

        public:
            virtual lisp_prims::STRING get_repr(void) = 0;
    } *any;

    typedef
    class _any_atom : public _any {
        protected:
            bool symbol;

        public:
            virtual lisp_prims::STRING get_repr(void) override = 0;
            virtual bool is_symbol(void) = 0;
    } *any_atom;

    // Can be bool, number, or string
    template <class T>
    class atom : public _any_atom {
        private:
            T value;
            void set_repr(T atom_value);

        public:
            atom(T atom_value, bool is_symbol=false);
            T get_value(void);
            lisp_prims::STRING get_repr(void) override;
            bool is_symbol(void);
    };

    // List of atoms or other lists
    class list : public _any {
        private:
            std::list<any> value;
            bool literal;
            void set_repr(std::list<any> list_value);

        public:
            typedef std::list<any>::iterator iterator;
            list(void);
            list(std::list<any> list_value, bool is_literal=false);
            std::list<any> get_value(void);
            any front(void);
            iterator begin(void);
            iterator end(void);
            lisp_prims::STRING get_repr(void) override;
            bool is_literal(void);
            int length(void);
    };
}

#endif