#ifndef LISP_UTILS_HPP_
#define LISP_UTILS_HPP_

#include <lisp_types.hpp>

namespace lisp_utils
{
    // Format checking
    bool is_compressed_c_r(lisp_prims::STRING c_r_string);

    // Type checking
    bool is_atom(lisp_types::any obj);
    bool is_atom_bool(lisp_types::any obj);
    bool is_atom_int(lisp_types::any obj);
    bool is_atom_float(lisp_types::any obj);
    bool is_atom_string(lisp_types::any obj);
    bool is_atom_symbol(lisp_types::any obj);

    bool is_list(lisp_types::any obj);
    bool is_list_nonliteral(lisp_types::any obj);


    // Type casting
    lisp_types::any      to_any(lisp_types::any_atom obj);
    lisp_types::any      to_any(lisp_types::list* obj);

    lisp_types::any_atom to_atom(lisp_types::any obj);
    lisp_types::list*    to_list(lisp_types::any obj);

    // Atom deduction
    lisp_types::atom<lisp_prims::BOOL>      to_atom_bool(lisp_types::any obj);
    lisp_types::atom<lisp_prims::BOOL>      to_atom_bool(lisp_types::any_atom obj);

    lisp_types::atom<lisp_prims::INTEGER32> to_atom_int(lisp_types::any obj);
    lisp_types::atom<lisp_prims::INTEGER32> to_atom_int(lisp_types::any_atom obj);

    lisp_types::atom<lisp_prims::FLOAT64>   to_atom_float(lisp_types::any obj);
    lisp_types::atom<lisp_prims::FLOAT64>   to_atom_float(lisp_types::any_atom obj);

    lisp_types::atom<lisp_prims::STRING>    to_atom_string(lisp_types::any obj);
    lisp_types::atom<lisp_prims::STRING>    to_atom_string(lisp_types::any_atom obj);
}

#endif