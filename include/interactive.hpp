#ifndef INTERACTIVE_HPP_
#define INTERACTIVE_HPP_

#include <lisp_types.hpp>
#include <string>

namespace lisp_interactive
{
    // Main read-eval-print-loop routine
    int repl(int argc, char **argv);

    class prompt {
        private:
            const char *prompt_pattern;
            int counter;

        public:
            prompt(void);
            void print_intro(void);
            void print_prompt(void);
            bool print_output(lisp_types::any expr);
            void print_help(void);
            void print_error(std::string error_msg);
            void quit_prompt(void);
            std::string read_input(void);
    };
}

#endif