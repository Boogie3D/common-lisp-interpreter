#ifndef READER_HPP_
#define READER_HPP_

#include <lisp_types.hpp>
#include <string>

namespace lisp_trans
{
    lisp_types::any      read_expr(std::string input,      bool is_literal=false);
    lisp_types::any_atom read_atom(std::string input_atom, bool is_symbol);
    lisp_types::list*    read_list(std::string input_list, bool is_literal);
}

#endif