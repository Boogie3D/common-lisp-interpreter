#ifndef SYMBOL_TABLE_HPP_
#define SYMBOL_TABLE_HPP_

#include <lisp_types.hpp>
#include <forward_list>
#include <map>

namespace lisp_stack
{
    struct symbol_t {
        bool is_function;
        lisp_types::list* func_params;
        lisp_types::list* func_body;
        lisp_types::any symbol_value;
        symbol_t(void);
        symbol_t(lisp_types::any value);
        symbol_t(lisp_types::list* params, lisp_types::list* body);
    };

    class symbol_table {
        private:
            std::map<lisp_prims::STRING, symbol_t> table;

        public:
            bool empty(void);
            bool has_symbol(lisp_types::atom<lisp_prims::STRING> symbol);
            void add_symbol(lisp_types::atom<lisp_prims::STRING> symbol,
                lisp_types::any value);
            void add_function(lisp_types::atom<lisp_prims::STRING> function,
                lisp_types::list params, lisp_types::list body);
            void add_builtin_function(lisp_types::atom<lisp_prims::STRING> function);
            symbol_t* get_symbol(lisp_types::atom<lisp_prims::STRING> symbol);
            void set_symbol(lisp_types::atom<lisp_prims::STRING> symbol, lisp_types::any value);
            void set_function(lisp_types::atom<lisp_prims::STRING> symbol,
                lisp_types::list params, lisp_types::list body);
    };

    class call_stack {
        private:
            static call_stack call_stack_instance;
            std::forward_list<symbol_table> stack_frame;

        public:
            static call_stack& get_instance(void);
            static void initialize(void);
            void push_symbol_table(symbol_table table);
            void pop_symbol_table(void);
            symbol_table* peek_symbol_table(void);
            symbol_t* find_symbol(lisp_types::atom<lisp_prims::STRING> symbol);
    };
}

#endif