#ifndef FUNC_CALL_HPP_
#define FUNC_CALL_HPP_

#include <lisp_types.hpp>

namespace lisp_funcs
{
    lisp_types::any call_builtin_func(lisp_types::list* expr);
    lisp_types::any call_user_func(lisp_types::list* expr);
}

#endif