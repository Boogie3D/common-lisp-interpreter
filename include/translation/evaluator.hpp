#ifndef EVALUATOR_HPP_
#define EVALUATOR_HPP_

#include <lisp_types.hpp>

namespace lisp_trans
{
    lisp_types::any evaluate(lisp_types::any expr);
    lisp_types::any evaluate(lisp_types::any_atom expr);
    lisp_types::any evaluate(lisp_types::list* expr);
}

#endif