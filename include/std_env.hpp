#ifndef STD_ENV_HPP_
#define STD_ENV_HPP_

#include <lisp_types.hpp>
#include <translation/symbol_table.hpp>

namespace lisp_std_env
{
    /* Begin built-in constructs */
    lisp_types::any if_stmt(lisp_types::atom<lisp_prims::BOOL> cond, lisp_types::any then_expr, lisp_types::any else_expr);
    lisp_types::any define(lisp_types::atom<lisp_prims::STRING> symbol, lisp_types::any value);
    lisp_types::any set(lisp_types::atom<lisp_prims::STRING> symbol, lisp_types::any value);
    lisp_types::any defun(lisp_types::atom<lisp_prims::STRING> func, lisp_types::list params, lisp_types::list body);
    /* End built-in constructs */


    /* Begin built-in operations */
    // Addition
    lisp_types::any_atom add(lisp_types::any_atom a, lisp_types::any_atom b);

    // Subtraction
    lisp_types::any_atom sub(lisp_types::any_atom a, lisp_types::any_atom b);

    // Multiplication
    lisp_types::any_atom mult(lisp_types::any_atom a, lisp_types::any_atom b);

    // Division
    lisp_types::any_atom div(lisp_types::any_atom a, lisp_types::any_atom b);
    /* End built-in operations */


    /* Begin built-in functions */
    // List manipulation functions
    lisp_types::any   car(lisp_types::list* lst);
    lisp_types::list* cdr(lisp_types::list* lst);
    lisp_types::any   compressed_c_r(lisp_prims::STRING c_r, lisp_types::list* lst);
    lisp_types::list* cons(lisp_types::any value, lisp_types::list* lst);

    // Math functions
    lisp_types::atom<lisp_prims::FLOAT64>* sqrt(lisp_types::any_atom atm);
    lisp_types::any_atom pow(lisp_types::any_atom base, lisp_types::any_atom power);
    /* End built-in functions */


    /* Begin built-in relations */
    // Greater than
    lisp_types::atom<lisp_prims::BOOL>* gt(lisp_types::any_atom a, lisp_types::any_atom b);

    // Greater than or equal to
    lisp_types::atom<lisp_prims::BOOL>* gte(lisp_types::any_atom a, lisp_types::any_atom b);

    // Less than
    lisp_types::atom<lisp_prims::BOOL>* lt(lisp_types::any_atom a, lisp_types::any_atom b);

    // Less than or equal to
    lisp_types::atom<lisp_prims::BOOL>* lte(lisp_types::any_atom a, lisp_types::any_atom b);

    // Equal
    lisp_types::atom<lisp_prims::BOOL>* eq(lisp_types::any_atom a, lisp_types::any_atom b);

    // Not equal
    lisp_types::atom<lisp_prims::BOOL>* neq(lisp_types::any_atom a, lisp_types::any_atom b);

    // Logical AND
    lisp_types::atom<lisp_prims::BOOL>* lisp_and(lisp_types::atom<lisp_prims::BOOL> a, lisp_types::atom<lisp_prims::BOOL> b);

    // Logical OR
    lisp_types::atom<lisp_prims::BOOL>* lisp_or(lisp_types::atom<lisp_prims::BOOL> a, lisp_types::atom<lisp_prims::BOOL> b);

    // Logical NOT
    lisp_types::atom<lisp_prims::BOOL>* lisp_not(lisp_types::atom<lisp_prims::BOOL> a);
    /* End built-in relations */
}

#endif