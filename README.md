# LispInt

A simplistic common Lisp Interpreter written in C++. All functionalities are
consistent with what is expected in the project instructions PDF. Note that
except that `define` will refuse to define a new variable if it already exists.

To run the program, type `make` and then run the executable created in the `bin` directory.

A log is created in the base directory, called `lispint_session.log`.

## Supported types
* atom (string)
* atom (symbol)
* atom (long integer)
* atom (double)
* atom (boolean)
* list

## Supported commands
* `define`: evaluate an expression and assign to a new variable
* `set!`: evaluate an expression and assign to an existing variable
* `defun`: define a new function
* `if`: if test evaluates to true then evaluate consequence, else alt
* `help`: print a help message
* `quit`: leave the interpreter

## Supported operators
* `+`: addition
* `-`: subtraction
* `*`: multiplication
* `/`: division

## Supported functions
### List manipulators
* `car`: get first element of a list
* `cdr`: get all but first element of a list
* `compressed c(a|d)*r`: convenient shorthand for nested `car`/`cdr`

### Mathematical functions
* `sqrt`: compute square root
* `pow`: compute base to a power

## Supported relational operators
### Comparators (can compare strings too)
* `>`
* `>=`
* `<`
* `<=`
* `=`
* `!=`

### Boolean operators
* `and`
* `or`
* `not`

## Features not Supported
* `BigInt`: integers with more than 64 bits
* `mapcar`: apply function to elements of one or more lists
* `lambda`: create anonymous function