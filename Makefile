# Executable name
TARGET_EXEC := lispint

# Project directories
BIN_DIR := ./bin
BUILD_DIR := ./build
INC_DIR := ./include
SRC_DIR := ./src

# Module directories
MODULES := $(shell find $(SRC_DIR) -mindepth 1 -type d -exec basename {} \;)
SRC_DIRS := $(addprefix $(SRC_DIR)/,$(MODULES))

# Source code and object files
SRCS := $(wildcard $(SRC_DIR)/*.cpp) $(foreach sdir,$(SRC_DIRS),$(wildcard $(sdir)/*.cpp))
OBJS := $(patsubst $(SRC_DIR)/%.cpp,$(BUILD_DIR)/%.o,$(SRCS))

# Include, enforce header pathnames relative to $(INC_DIR)
INC_FLAGS := -I$(INC_DIR)

# Executable/object compilation
CXX := g++
CXXFLAGS := -g -Wall $(INC_FLAGS)

LD := $(CXX)
LDFLAGS := $(CXXFLAGS)

# Commands
MKDIR ?= mkdir -p
RM := rm -rf

.PHONY: all
all: $(BIN_DIR)/$(TARGET_EXEC)

# Make executable
$(BIN_DIR)/$(TARGET_EXEC): $(OBJS)
	@mkdir -p $(BIN_DIR)
	$(LD) $(LDFLAGS) $^ -o $@
	@echo
	@echo "The executable '$(TARGET_EXEC)' has been generated in the '$(BIN_DIR)' directory."

# Make objects
$(OBJS):$(BUILD_DIR)/%.o: $(SRC_DIR)/%.cpp
	@mkdir -p $(dir $@)
	$(CXX) $(CXXFLAGS) -c $< -o $@

.PHONY: clean
# Remove $(BUILD_DIR) and the target executable
clean:
	$(RM) $(BUILD_DIR) $(BIN_DIR)/$(TARGET_EXEC)
