#include <lisp_types.hpp>
#include <list>
using namespace lisp_prims;

namespace lisp_types
{
    list::list() {
        list(std::list<any>(), true);
    }

    list::list(std::list<any> list_value, bool is_literal) {
        value = list_value;
        literal = is_literal;
        set_repr(list_value);
    }

    void list::set_repr(std::list<any> list_value) {
        if (length() == 0) {
            repr = "NIL";
        } else {
            STRING repr_string;
            repr_string = "(";
            for (auto it : list_value) {
                repr_string += (*it).get_repr() + " ";
            }
            repr_string[repr_string.size() - 1] = ')';
            repr = repr_string;
        }
    }

    std::list<any> list::get_value() {
        return value;
    }

    any list::front() {
        return value.front();
    }

    list::iterator list::begin() {
        return value.begin();
    }

    list::iterator list::end() {
        return value.end();
    }

    STRING list::get_repr() {
        return repr;
    }

    bool list::is_literal() {
        return literal;
    }

    int list::length() {
        return get_value().size();
    }
}