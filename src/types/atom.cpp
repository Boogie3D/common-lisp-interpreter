#include <lisp_types.hpp>
#include <string>
using namespace lisp_prims;

namespace lisp_types
{
    template class atom<BOOL>;
    template class atom<INTEGER32>;
    template class atom<FLOAT64>;
    template class atom<STRING>;

    template <class T>
    atom<T>::atom(T atom_value, bool is_symbol) {
        value = atom_value;
        symbol = is_symbol;
        set_repr(atom_value);
    }

    template <class T>
    T atom<T>::get_value() {
        return value;
    }

    template <>
    void atom<BOOL>::set_repr(BOOL atom_value) {
        repr = (atom_value == BOOL::NIL) ? "NIL" : "T";
    }

    template <>
    void atom<INTEGER32>::set_repr(INTEGER32 atom_value) {
        repr = std::to_string(atom_value);
    }

    template <>
    void atom<FLOAT64>::set_repr(FLOAT64 atom_value) {
        repr = std::to_string(atom_value);
    }

    template <>
    void atom<STRING>::set_repr(STRING atom_value) {
        repr = atom_value;
    }

    template <class T>
    STRING atom<T>::get_repr() {
        return repr;
    }

    template <class T>
    bool atom<T>::is_symbol() {
        return symbol;
    }
}