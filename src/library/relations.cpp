#include <std_env.hpp>
#include <lisp_exceptions.hpp>
#include <lisp_types.hpp>
#include <lisp_utils.hpp>
#include <string>
using namespace lisp_exceptions;
using namespace lisp_prims;
using namespace lisp_types;
using namespace lisp_utils;

namespace lisp_std_env
{
    // Greater than
    atom<BOOL>* gt(any_atom a, any_atom b) {
        if (is_atom_int(a) && is_atom_int(b)) {
            return to_atom_int(a).get_value() > to_atom_int(b).get_value() ?
                new atom<BOOL>(BOOL::T) : new atom<BOOL>(BOOL::NIL);
        } else if (is_atom_int(a) && is_atom_float(b)) { 
            return to_atom_int(a).get_value() > to_atom_float(b).get_value() ?
                new atom<BOOL>(BOOL::T) : new atom<BOOL>(BOOL::NIL);
        } else if (is_atom_float(a) && is_atom_int(b)) {
            return to_atom_float(a).get_value() > to_atom_int(b).get_value() ?
                new atom<BOOL>(BOOL::T) : new atom<BOOL>(BOOL::NIL);
        } else if (is_atom_float(a) && is_atom_float(b)) {
            return to_atom_float(a).get_value() > to_atom_float(b).get_value() ?
                new atom<BOOL>(BOOL::T) : new atom<BOOL>(BOOL::NIL);
        } else if (is_atom_string(a) && is_atom_string(b)) {
            return to_atom_string(a).get_value().compare(to_atom_string(b).get_value()) > 0 ?
                new atom<BOOL>(BOOL::T) : new atom<BOOL>(BOOL::NIL);
        } else {
            throw invalid_argument("'" + a->get_repr() + "' and '" + b->get_repr() + "' are not comparable");
        }
    }

    // Greater than or equal to
    atom<BOOL>* gte(any_atom a, any_atom b) {
        if (is_atom_int(a) && is_atom_int(b)) {
            return to_atom_int(a).get_value() >= to_atom_int(b).get_value() ?
                new atom<BOOL>(BOOL::T) : new atom<BOOL>(BOOL::NIL);
        } else if (is_atom_int(a) && is_atom_float(b)) { 
            return to_atom_int(a).get_value() >= to_atom_float(b).get_value() ?
                new atom<BOOL>(BOOL::T) : new atom<BOOL>(BOOL::NIL);
        } else if (is_atom_float(a) && is_atom_int(b)) {
            return to_atom_float(a).get_value() >= to_atom_int(b).get_value() ?
                new atom<BOOL>(BOOL::T) : new atom<BOOL>(BOOL::NIL);
        } else if (is_atom_float(a) && is_atom_float(b)) {
            return to_atom_float(a).get_value() >= to_atom_float(b).get_value() ?
                new atom<BOOL>(BOOL::T) : new atom<BOOL>(BOOL::NIL);
        } else if (is_atom_string(a) && is_atom_string(b)) {
            return to_atom_string(a).get_value().compare(to_atom_string(b).get_value()) >= 0 ?
                new atom<BOOL>(BOOL::T) : new atom<BOOL>(BOOL::NIL);
        } else {
            throw invalid_argument("'" + a->get_repr() + "' and '" + b->get_repr() + "' are not comparable");
        }
    }

    // Less than
    atom<BOOL>* lt(any_atom a, any_atom b) {
        if (is_atom_int(a) && is_atom_int(b)) {
            return to_atom_int(a).get_value() < to_atom_int(b).get_value() ?
                new atom<BOOL>(BOOL::T) : new atom<BOOL>(BOOL::NIL);
        } else if (is_atom_int(a) && is_atom_float(b)) { 
            return to_atom_int(a).get_value() < to_atom_float(b).get_value() ?
                new atom<BOOL>(BOOL::T) : new atom<BOOL>(BOOL::NIL);
        } else if (is_atom_float(a) && is_atom_int(b)) {
            return to_atom_float(a).get_value() < to_atom_int(b).get_value() ?
                new atom<BOOL>(BOOL::T) : new atom<BOOL>(BOOL::NIL);
        } else if (is_atom_float(a) && is_atom_float(b)) {
            return to_atom_float(a).get_value() < to_atom_float(b).get_value() ?
                new atom<BOOL>(BOOL::T) : new atom<BOOL>(BOOL::NIL);
        } else if (is_atom_string(a) && is_atom_string(b)) {
            return to_atom_string(a).get_value().compare(to_atom_string(b).get_value()) < 0 ?
                new atom<BOOL>(BOOL::T) : new atom<BOOL>(BOOL::NIL);
        } else {
            throw invalid_argument("'" + a->get_repr() + "' and '" + b->get_repr() + "' are not comparable");
        }
    }

    // Less than or equal to
    atom<BOOL>* lte(any_atom a, any_atom b) {
        if (is_atom_int(a) && is_atom_int(b)) {
            return to_atom_int(a).get_value() <= to_atom_int(b).get_value() ?
                new atom<BOOL>(BOOL::T) : new atom<BOOL>(BOOL::NIL);
        } else if (is_atom_int(a) && is_atom_float(b)) { 
            return to_atom_int(a).get_value() <= to_atom_float(b).get_value() ?
                new atom<BOOL>(BOOL::T) : new atom<BOOL>(BOOL::NIL);
        } else if (is_atom_float(a) && is_atom_int(b)) {
            return to_atom_float(a).get_value() <= to_atom_int(b).get_value() ?
                new atom<BOOL>(BOOL::T) : new atom<BOOL>(BOOL::NIL);
        } else if (is_atom_float(a) && is_atom_float(b)) {
            return to_atom_float(a).get_value() <= to_atom_float(b).get_value() ?
                new atom<BOOL>(BOOL::T) : new atom<BOOL>(BOOL::NIL);
        } else if (is_atom_string(a) && is_atom_string(b)) {
            return to_atom_string(a).get_value().compare(to_atom_string(b).get_value()) <= 0 ?
                new atom<BOOL>(BOOL::T) : new atom<BOOL>(BOOL::NIL);
        } else {
            throw invalid_argument("'" + a->get_repr() + "' and '" + b->get_repr() + "' are not comparable");
        }
    }

    // Equal
    atom<BOOL>* eq(any_atom a, any_atom b) {
        if (is_atom_int(a) && is_atom_int(b)) {
            return to_atom_int(a).get_value() == to_atom_int(b).get_value() ?
                new atom<BOOL>(BOOL::T) : new atom<BOOL>(BOOL::NIL);
        } else if (is_atom_int(a) && is_atom_float(b)) { 
            return to_atom_int(a).get_value() == to_atom_float(b).get_value() ?
                new atom<BOOL>(BOOL::T) : new atom<BOOL>(BOOL::NIL);
        } else if (is_atom_float(a) && is_atom_int(b)) {
            return to_atom_float(a).get_value() == to_atom_int(b).get_value() ?
                new atom<BOOL>(BOOL::T) : new atom<BOOL>(BOOL::NIL);
        } else if (is_atom_float(a) && is_atom_float(b)) {
            return to_atom_float(a).get_value() == to_atom_float(b).get_value() ?
                new atom<BOOL>(BOOL::T) : new atom<BOOL>(BOOL::NIL);
        } else if (is_atom_string(a) && is_atom_string(b)) {
            return to_atom_string(a).get_value().compare(to_atom_string(b).get_value()) == 0 ?
                new atom<BOOL>(BOOL::T) : new atom<BOOL>(BOOL::NIL);
        } else if (is_atom_bool(a) && is_atom_bool(b)) {
            return to_atom_bool(a).get_value() == to_atom_bool(b).get_value() ?
                new atom<BOOL>(BOOL::T) : new atom<BOOL>(BOOL::NIL);
        } else {
            throw invalid_argument("'" + a->get_repr() + "' and '" + b->get_repr() + "' are not comparable");
        }
    }

    // Not equal
    atom<BOOL>* neq(any_atom a, any_atom b) {
        if (is_atom_int(a) && is_atom_int(b)) {
            return to_atom_int(a).get_value() != to_atom_int(b).get_value() ?
                new atom<BOOL>(BOOL::T) : new atom<BOOL>(BOOL::NIL);
        } else if (is_atom_int(a) && is_atom_float(b)) { 
            return to_atom_int(a).get_value() != to_atom_float(b).get_value() ?
                new atom<BOOL>(BOOL::T) : new atom<BOOL>(BOOL::NIL);
        } else if (is_atom_float(a) && is_atom_int(b)) {
            return to_atom_float(a).get_value() != to_atom_int(b).get_value() ?
                new atom<BOOL>(BOOL::T) : new atom<BOOL>(BOOL::NIL);
        } else if (is_atom_float(a) && is_atom_float(b)) {
            return to_atom_float(a).get_value() != to_atom_float(b).get_value() ?
                new atom<BOOL>(BOOL::T) : new atom<BOOL>(BOOL::NIL);
        } else if (is_atom_string(a) && is_atom_string(b)) {
            return to_atom_string(a).get_value().compare(to_atom_string(b).get_value()) != 0 ?
                new atom<BOOL>(BOOL::T) : new atom<BOOL>(BOOL::NIL);
        } else if (is_atom_bool(a) && is_atom_bool(b)) {
            return to_atom_bool(a).get_value() != to_atom_bool(b).get_value() ?
                new atom<BOOL>(BOOL::T) : new atom<BOOL>(BOOL::NIL);
        } else {
            throw invalid_argument("'" + a->get_repr() + "' and '" + b->get_repr() + "' are not comparable");
        }
    }

    // Logical AND
    atom<BOOL>* lisp_and(atom<BOOL> a, atom<BOOL> b) {
        return a.get_value() ? new atom<BOOL>(b.get_value()) : new atom<BOOL>(a.get_value());
    }

    // Logical OR
    atom<BOOL>* lisp_or(atom<BOOL> a, atom<BOOL> b) {
        return a.get_value() ? new atom<BOOL>(a.get_value()) : new atom<BOOL>(b.get_value());
    }

    // Logical NOT
    atom<BOOL>* lisp_not(atom<BOOL> a) {
        return a.get_value() ? new atom<BOOL>(BOOL::NIL) : new atom<BOOL>(BOOL::T);
    }
}