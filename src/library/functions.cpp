#include <lisp_exceptions.hpp>
#include <lisp_utils.hpp>
#include <std_env.hpp>
#include <cmath>
#include <list>
using namespace lisp_exceptions;
using namespace lisp_prims;
using namespace lisp_types;
using namespace lisp_utils;

namespace lisp_std_env
{
    any car(list* lst) {
        if (lst->length() == 0)
            return lst;
        return lst->front();
    }

    list* cdr(list* lst) {
        if (lst->length() == 0)
            return lst;
        std::list<any> cdr_list(++lst->begin(), lst->end());
        return new list(cdr_list);
    }

    any compressed_c_r(STRING c_r, list* lst) {
        any c_r_iterate = lst;

        auto it = c_r.rbegin() + 1;
        for (; it != c_r.rend() - 1; it++) {
            if (!is_list(c_r_iterate))
                throw invalid_argument("'" + c_r_iterate->get_repr() + "' is not a list");

            if (*it == 'A') {
                c_r_iterate = car(to_list(c_r_iterate));
            } else if (*it == 'D') {
                c_r_iterate = cdr(to_list(c_r_iterate));
            }
        }

        return c_r_iterate;
    }

    list* cons(any value, list* lst) {
        std::list<any> cons_list = lst->get_value();
        cons_list.push_front(value);
        return new list(cons_list);
    }

    // Math functions
    atom<FLOAT64>* sqrt(any_atom value) {
        if (is_atom_bool(value) || is_atom_string(value))
            throw invalid_argument("'" + value->get_repr() + "' is not a number");
        
        if (is_atom_int(value))
            return new atom<FLOAT64>(std::sqrt(to_atom_int(value).get_value()));
        else
            return new atom<FLOAT64>(std::sqrt(to_atom_float(value).get_value()));
    }

    any_atom pow(any_atom base, any_atom power) {
        if (is_atom_bool(base) || is_atom_string(base))
            throw invalid_argument(base->get_repr() + "' is not a number");

        if (is_atom_bool(power) || is_atom_string(power))
            throw invalid_argument(power->get_repr() + "' is not a number");

        if (is_atom_int(base) && is_atom_int(power)) {
            return new atom<INTEGER32>(std::pow(to_atom_int(base).get_value(),
                to_atom_int(power).get_value()));
        } else if (is_atom_int(base) && is_atom_float(power)) {
            return new atom<FLOAT64>(std::pow(to_atom_int(base).get_value(),
                to_atom_float(power).get_value()));
        } else if (is_atom_float(base) && is_atom_int(power)) {
            return new atom<FLOAT64>(std::pow(to_atom_float(base).get_value(),
                to_atom_int(power).get_value()));
        } else {
            return new atom<FLOAT64>(std::pow(to_atom_float(base).get_value(),
                to_atom_float(power).get_value()));
        }
    }
}