#include <lisp_exceptions.hpp>
#include <lisp_types.hpp>
#include <lisp_utils.hpp>
#include <std_env.hpp>
using namespace lisp_exceptions;
using namespace lisp_prims;
using namespace lisp_types;
using namespace lisp_utils;

namespace lisp_std_env
{
    any_atom add(any_atom a, any_atom b) {
        if (is_atom_bool(a) || is_atom_string(a))
            throw invalid_argument("'" + a->get_repr() + "'" + " is not a number");

        if (is_atom_bool(b) || is_atom_string(b))
            throw invalid_argument("'" + b->get_repr() + "'" + " is not a number");

        if (is_atom_int(a) && is_atom_int(b)) {
            INTEGER32 a_val = to_atom_int(a).get_value();
            INTEGER32 b_val = to_atom_int(b).get_value();
            return new atom<INTEGER32>(a_val + b_val);
        } else if (is_atom_int(a) && is_atom_float(b)) {
            INTEGER32 a_val = to_atom_int(a).get_value();
            FLOAT64 b_val = to_atom_float(b).get_value();
            return new atom<FLOAT64>(a_val + b_val);
        } else if (is_atom_float(a) && is_atom_int(b)) {
            FLOAT64 a_val = to_atom_float(a).get_value();
            INTEGER32 b_val = to_atom_int(b).get_value();
            return new atom<FLOAT64>(a_val + b_val);
        } else {
            FLOAT64 a_val = to_atom_float(a).get_value();
            FLOAT64 b_val = to_atom_float(b).get_value();
            return new atom<FLOAT64>(a_val + b_val);
        }
    }

    any_atom sub(any_atom a, any_atom b) {
        if (is_atom_bool(a) || is_atom_string(a))
            throw invalid_argument("'" + a->get_repr() + "'" + " is not a number");

        if (is_atom_bool(b) || is_atom_string(b))
            throw invalid_argument("'" + b->get_repr() + "'" + " is not a number");

        if (is_atom_int(a) && is_atom_int(b)) {
            INTEGER32 a_val = to_atom_int(a).get_value();
            INTEGER32 b_val = to_atom_int(b).get_value();
            return new atom<INTEGER32>(a_val - b_val);
        } else if (is_atom_int(a) && is_atom_float(b)) {
            INTEGER32 a_val = to_atom_int(a).get_value();
            FLOAT64 b_val = to_atom_float(b).get_value();
            return new atom<FLOAT64>(a_val - b_val);
        } else if (is_atom_float(a) && is_atom_int(b)) {
            FLOAT64 a_val = to_atom_float(a).get_value();
            INTEGER32 b_val = to_atom_int(b).get_value();
            return new atom<FLOAT64>(a_val - b_val);
        } else {
            FLOAT64 a_val = to_atom_float(a).get_value();
            FLOAT64 b_val = to_atom_float(b).get_value();
            return new atom<FLOAT64>(a_val - b_val);
        }
    }

    any_atom mult(any_atom a, any_atom b) {
        if (is_atom_bool(a) || is_atom_string(a))
            throw invalid_argument("'" + a->get_repr() + "'" + " is not a number");

        if (is_atom_bool(b) || is_atom_string(b))
            throw invalid_argument("'" + b->get_repr() + "'" + " is not a number");

        if (is_atom_int(a) && is_atom_int(b)) {
            INTEGER32 a_val = to_atom_int(a).get_value();
            INTEGER32 b_val = to_atom_int(b).get_value();
            return new atom<INTEGER32>(a_val * b_val);
        } else if (is_atom_int(a) && is_atom_float(b)) {
            INTEGER32 a_val = to_atom_int(a).get_value();
            FLOAT64 b_val = to_atom_float(b).get_value();
            return new atom<FLOAT64>(a_val * b_val);
        } else if (is_atom_float(a) && is_atom_int(b)) {
            FLOAT64 a_val = to_atom_float(a).get_value();
            INTEGER32 b_val = to_atom_int(b).get_value();
            return new atom<FLOAT64>(a_val * b_val);
        } else {
            FLOAT64 a_val = to_atom_float(a).get_value();
            FLOAT64 b_val = to_atom_float(b).get_value();
            return new atom<FLOAT64>(a_val * b_val);
        }
    }

    any_atom div(any_atom a, any_atom b) {
        if (is_atom_bool(a) || is_atom_string(a))
            throw invalid_argument("'" + a->get_repr() + "'" + " is not a number");

        if (is_atom_bool(b) || is_atom_string(b))
            throw invalid_argument("'" + b->get_repr() + "'" + " is not a number");

        if (is_atom_int(a) && is_atom_int(b)) {
            INTEGER32 a_val = to_atom_int(a).get_value();
            INTEGER32 b_val = to_atom_int(b).get_value();

            if (b_val == 0)
                throw illegal_operation("cannot divide by zero");

            return new atom<INTEGER32>(a_val / b_val);
        } else if (is_atom_int(a) && is_atom_float(b)) {
            INTEGER32 a_val = to_atom_int(a).get_value();
            FLOAT64 b_val = to_atom_float(b).get_value();
            if (b_val == 0)
                throw illegal_operation("cannot divide by zero");

            return new atom<FLOAT64>(a_val / b_val);
        } else if (is_atom_float(a) && is_atom_int(b)) {
            FLOAT64 a_val = to_atom_float(a).get_value();
            INTEGER32 b_val = to_atom_int(b).get_value();
            if (b_val == 0)
                throw illegal_operation("cannot divide by zero");

            return new atom<FLOAT64>(a_val / b_val);
        } else {
            FLOAT64 a_val = to_atom_float(a).get_value();
            FLOAT64 b_val = to_atom_float(b).get_value();
            if (b_val == 0)
                throw illegal_operation("cannot divide by zero");

            return new atom<FLOAT64>(a_val / b_val);
        }
    }
}
