#include <lisp_exceptions.hpp>
#include <lisp_types.hpp>
#include <lisp_utils.hpp>
#include <std_env.hpp>
#include <translation/evaluator.hpp>
#include <translation/symbol_table.hpp>
using namespace lisp_exceptions;
using namespace lisp_prims;
using namespace lisp_stack;
using namespace lisp_trans;
using namespace lisp_types;
using namespace lisp_utils;

namespace lisp_std_env
{
    any if_stmt(atom<BOOL> cond, any then_expr, any else_expr) {
        if (cond.get_value() == BOOL::T) {
            return evaluate(then_expr);
        } else {
            return evaluate(else_expr);
        }
    }

    any define(atom<STRING> symbol, any value) {
        call_stack stack = call_stack::get_instance();
        stack.peek_symbol_table()->add_symbol(symbol, value);
        return value;
    }

    any set(atom<STRING> symbol, any value) {
        call_stack stack = call_stack::get_instance();
        stack.peek_symbol_table()->set_symbol(symbol, value);
        return value;
    }

    any defun(atom<STRING> func, list params, list body) {
        call_stack stack = call_stack::get_instance();
        if (stack.peek_symbol_table()->has_symbol(func))
            stack.peek_symbol_table()->set_function(func, params, body);
        else
            stack.peek_symbol_table()->add_function(func, params, body);

        return to_any(new atom<STRING>(func.get_value()));
    }
}
