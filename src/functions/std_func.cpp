#include <lisp_exceptions.hpp>
#include <lisp_types.hpp>
#include <lisp_utils.hpp>
#include <std_env.hpp>
#include <translation/evaluator.hpp>
#include <translation/func_call.hpp>
#include <vector>
using namespace lisp_exceptions;
using namespace lisp_prims;
using namespace lisp_stack;
using namespace lisp_std_env;
using namespace lisp_trans;
using namespace lisp_types;
using namespace lisp_utils;

namespace lisp_funcs
{
    any call_builtin_func(list* expr) {
        auto param = ++expr->begin();
        std::vector<any> params;

        while (param != expr->end()) {
            params.push_back(*param);
            param++;
        }

        STRING func_name = to_atom_string(expr->front()).get_value();
        if (func_name == "QUIT") {
            if (params.size() > 0)
                throw invalid_call("QUIT", 0);

            return to_any(expr);
        } else if (func_name == "HELP") {
            if (params.size() > 0)
                throw invalid_call("HELP", 0);

            return to_any(expr);
        } else if (func_name == "IF") {
            if (params.size() != 3)
                throw invalid_call("IF", 3);

            any cond = evaluate(params[0]);
            if (!is_atom_bool(cond))
                throw invalid_argument("'" + cond->get_repr() + "' is not an atom");

            return if_stmt(to_atom_bool(cond), params[1], params[2]);
        } else if (func_name == "DEFINE") {
            if (params.size() != 2)
                throw invalid_call("DEFINE", 2);

            if (!is_atom_symbol(params[0]))
                throw invalid_argument("'" + params[0]->get_repr() + " is not a symbol");

            return define(to_atom_string(params[0]), evaluate(params[1]));
        } else if (func_name == "SET!") {
            if (params.size() != 2)
                throw invalid_call("SET!", 2);

            if (!is_atom_symbol(params[0]))
                throw invalid_argument("'" + params[0]->get_repr() + " is not a symbol");

            return set(to_atom_string(params[0]), evaluate(params[1]));
        } else if (func_name == "DEFUN") {
            if (params.size() != 3)
                throw invalid_call("DEFUN", 3);

            if (!is_atom_symbol(params[0]))
                throw invalid_argument("'" + params[0]->get_repr() + " is not a symbol");

            if (!is_list_nonliteral(params[1]))
                throw invalid_argument("'" + params[1]->get_repr() + " is not a list of arguments");

            if (!is_list_nonliteral(params[2]))
                throw invalid_argument("'" + params[2]->get_repr() + " is not a function body (as a list)");

            return defun(to_atom_string(params[0]), *to_list(params[1]), *to_list(params[2]));
        } else if (func_name == "+") {
            if (params.size() != 2)
                throw invalid_call("+", 2);

            any a = evaluate(params[0]);
            any b = evaluate(params[1]);

            if (!is_atom(a))
                throw invalid_argument("'" + a->get_repr() + "' is not an atom");

            if (!is_atom(b))
                throw invalid_argument("'" + b->get_repr() + "' is not an atom");

            return add(to_atom(a), to_atom(b));
        } else if (func_name == "-") {
            if (params.size() != 2)
                throw invalid_call("-", 2);

            any a = evaluate(params[0]);
            any b = evaluate(params[1]);

            if (!is_atom(a))
                throw invalid_argument("'" + a->get_repr() + "' is not an atom");

            if (!is_atom(b))
                throw invalid_argument("'" + b->get_repr() + "' is not an atom");

            return sub(to_atom(a), to_atom(b));
        } else if (func_name == "*") {
            if (params.size() != 2)
                throw invalid_call("*", 2);

            any a = evaluate(params[0]);
            any b = evaluate(params[1]);

            if (!is_atom(a))
                throw invalid_argument("'" + a->get_repr() + "' is not an atom");

            if (!is_atom(b))
                throw invalid_argument("'" + b->get_repr() + "' is not an atom");

            return mult(to_atom(a), to_atom(b));
        } else if (func_name == "/") {
            if (params.size() != 2)
                throw invalid_call("/", 2);

            any a = evaluate(params[0]);
            any b = evaluate(params[1]);

            if (!is_atom(a))
                throw invalid_argument("'" + a->get_repr() + "' is not an atom");

            if (!is_atom(b))
                throw invalid_argument("'" + b->get_repr() + "' is not an atom");

            return div(to_atom(a), to_atom(b));
        } else if (func_name == "CAR") {
            if (params.size() != 1)
                throw invalid_call("CAR", 1);

            any val = evaluate(params[0]);
            if (!is_list(val))
                throw invalid_argument("'" + val->get_repr() + "' is not a list");

            return car(to_list(val));
        } else if (func_name == "CDR") {
            if (params.size() != 1)
                throw invalid_call("CDR", 1);

            any val = evaluate(params[0]);
            if (!is_list(val))
                throw invalid_argument("'" + val->get_repr() + "' is not a list");

            return cdr(to_list(val));
        } else if (func_name == "CONS") {
            if (params.size() != 2)
                throw invalid_call("CONS", 2);

            any val = evaluate(params[0]);
            any lst = evaluate(params[1]);

            if (!is_list(lst))
                throw invalid_argument("'" + lst->get_repr() + "' is not a list");

            return cons(val, to_list(lst));
        } else if (func_name == "SQRT") {
            if (params.size() != 1)
                throw invalid_call("SQRT", 1);

            any val = evaluate(params[0]);
            if (!is_atom(val))
                throw invalid_argument("'" + val->get_repr() + "' is not an atom");

            return sqrt(to_atom(val));
        } else if (func_name == "POW") {
            if (params.size() != 2)
                throw invalid_call("POW", 2);

            any base = evaluate(params[0]);
            any power = evaluate(params[1]);
            if (!is_atom(base))
                throw invalid_argument("'" + base->get_repr() + "' is not an atom");

            if (!is_atom(power))
                throw invalid_argument("'" + power->get_repr() + "' is not an atom");

            return pow(to_atom(base), to_atom(power));
        } else if (func_name == ">") {
            if (params.size() != 2)
                throw invalid_call(">", 2);

            any a = evaluate(params[0]);
            any b = evaluate(params[1]);
            if (!is_atom(a))
                throw invalid_argument("'" + a->get_repr() + "' is not an atom");

            if (!is_atom(b))
                throw invalid_argument("'" + b->get_repr() + "' is not an atom");

            return gt(to_atom(a), to_atom(b));
        } else if (func_name == ">=") {
            if (params.size() != 2)
                throw invalid_call(">=", 2);

            any a = evaluate(params[0]);
            any b = evaluate(params[1]);
            if (!is_atom(a))
                throw invalid_argument("'" + a->get_repr() + "' is not an atom");

            if (!is_atom(b))
                throw invalid_argument("'" + b->get_repr() + "' is not an atom");

            return gte(to_atom(a), to_atom(b));
        } else if (func_name == "<") {
            if (params.size() != 2) {
                throw invalid_call("<", 2);
            }
            any a = evaluate(params[0]);
            any b = evaluate(params[1]);
            if (!is_atom(a))
                throw invalid_argument("'" + a->get_repr() + "' is not an atom");

            if (!is_atom(b))
                throw invalid_argument("'" + b->get_repr() + "' is not an atom");

            return lt(to_atom(a), to_atom(b));
        } else if (func_name == "<=") {
            if (params.size() != 2) {
                throw invalid_call("<=", 2);
            }
            any a = evaluate(params[0]);
            any b = evaluate(params[1]);
            if (!is_atom(a))
                throw invalid_argument("'" + a->get_repr() + "' is not an atom");

            if (!is_atom(b))
                throw invalid_argument("'" + b->get_repr() + "' is not an atom");

            return lte(to_atom(a), to_atom(b));
        } else if (func_name == "=") {
            if (params.size() != 2)
                throw invalid_call("=", 2);

            any a = evaluate(params[0]);
            any b = evaluate(params[1]);

            if (!is_atom(a))
                throw invalid_argument("'" + a->get_repr() + "' is not an atom");

            if (!is_atom(b))
                throw invalid_argument("'" + b->get_repr() + "' is not an atom");

            return eq(to_atom(a), to_atom(b));
        } else if (func_name == "!=") {
            if (params.size() != 2)
                throw invalid_call("!=", 2);

            any a = evaluate(params[0]);
            any b = evaluate(params[1]);

            if (!is_atom(a))
                throw invalid_argument("'" + a->get_repr() + "' is not an atom");

            if (!is_atom(b))
                throw invalid_argument("'" + b->get_repr() + "' is not an atom");

            return neq(to_atom(a), to_atom(b));
        } else if (func_name == "AND") {
            if (params.size() != 2)
                throw invalid_call("AND", 2);

            any a = evaluate(params[0]);
            any b = evaluate(params[1]);

            if (!is_atom_bool(a))
                throw invalid_argument("'" + a->get_repr() + "' is not a boolean");

            if (!is_atom_bool(b))
                throw invalid_argument("'" + b->get_repr() + "' is not a boolean");

            return lisp_and(to_atom_bool(a), to_atom_bool(b));
        } else if (func_name == "OR") {
            if (params.size() != 2)
                throw invalid_call("OR", 2);

            any a = evaluate(params[0]);
            any b = evaluate(params[1]);

            if (!is_atom_bool(a))
                throw invalid_argument("'" + a->get_repr() + "' is not a boolean");

            if (!is_atom_bool(b))
                throw invalid_argument("'" + b->get_repr() + "' is not a boolean");

            return lisp_or(to_atom_bool(a), to_atom_bool(b));
        } else if (func_name == "NOT") {
            if (params.size() != 1)
                throw invalid_call("NOT", 1);

            any a = evaluate(params[0]);
            if (!is_atom_bool(a))
                throw invalid_argument("'" + a->get_repr() + "' is not a boolean");


            return lisp_not(to_atom_bool(a));
        } else if (is_compressed_c_r(func_name)) {
            if (params.size() != 1)
                throw invalid_call(func_name, 1);
            
            any lst = evaluate(params[0]);
            if (!is_list(lst))
                throw invalid_argument("'" + lst->get_repr() + "' is not a list");

            return compressed_c_r(func_name, to_list(lst));
        } else {
            // this shouldn't happen
            throw invalid_function("'" + func_name + "' is not a built-in function");
        }
    }
}
