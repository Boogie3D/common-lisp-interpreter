#include <lisp_exceptions.hpp>
#include <lisp_types.hpp>
#include <lisp_utils.hpp>
#include <translation/evaluator.hpp>
#include <translation/func_call.hpp>
#include <translation/symbol_table.hpp>
using namespace lisp_exceptions;
using namespace lisp_prims;
using namespace lisp_stack;
using namespace lisp_trans;
using namespace lisp_types;
using namespace lisp_utils;

namespace lisp_funcs
{
    any call_user_func(list* expr) {
        call_stack stack = call_stack::get_instance();
        atom<STRING> func = to_atom_string(expr->front());
        symbol_table table;
        symbol_t symbol;

        if (stack.find_symbol(func) == nullptr)
            throw invalid_function(&func);

        symbol = *stack.find_symbol(func);
        if (!symbol.is_function)
            throw invalid_function(func.get_repr() + " is not a function");

        if (expr->length() - 1 != symbol.func_params->length())
            throw invalid_call(func.get_repr(), symbol.func_body->length());

        auto param = ++expr->begin();
        auto param_symbol = symbol.func_params->begin();
        while (param != expr->end()) {
            if (table.has_symbol(to_atom_string(*param_symbol)))
                table.set_symbol(to_atom_string(*param_symbol), evaluate(*param));
            else
                table.add_symbol(to_atom_string(*param_symbol), evaluate(*param));
            param++;
            param_symbol++;
        }

        if (!table.empty())
            stack.push_symbol_table(table);
        
        any res = evaluate(symbol.func_body);
        
        if (!table.empty())
            stack.pop_symbol_table();
        
        return res;
    }
}