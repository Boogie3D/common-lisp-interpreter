#include <lisp_types.hpp>
using namespace lisp_prims;

namespace lisp_utils
{
    bool is_compressed_c_r(STRING c_r_string) {
        auto it = c_r_string.begin();

        if (it == c_r_string.end() || *it != 'C' || ++it == c_r_string.end())
            return false;

        while (it + 1 != c_r_string.end()) {
            if (*it != 'A' && *it != 'D')
                return false;
            it++;
        }

        return (*it == 'R') ? true : false;
    }
}