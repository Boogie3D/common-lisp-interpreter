#include <lisp_types.hpp>
#include <lisp_utils.hpp>
using namespace lisp_prims;
using namespace lisp_types;

namespace lisp_utils
{
    any to_any(any_atom obj) {
        return static_cast<any>(obj);
    }

    any to_any(list* obj) {
        return static_cast<any>(obj);
    }

    any_atom to_atom(any obj) {
        return static_cast<any_atom>(obj);
    }

    list* to_list(any obj) {
        return static_cast<list*>(obj);
    }

    atom<BOOL> to_atom_bool(any obj) {
        return *static_cast<atom<BOOL>*>(obj);
    }

    atom<BOOL> to_atom_bool(any_atom obj) {
        return *static_cast<atom<BOOL>*>(obj);
    }

    atom<INTEGER32> to_atom_int(any obj) {
        return *static_cast<atom<INTEGER32>*>(obj);
    }

    atom<INTEGER32> to_atom_int(any_atom obj) {
        return *static_cast<atom<INTEGER32>*>(obj);
    }

    atom<FLOAT64> to_atom_float(any obj) {
        return *static_cast<atom<FLOAT64>*>(obj);
    }

    atom<FLOAT64> to_atom_float(any_atom obj) {
        return *static_cast<atom<FLOAT64>*>(obj);
    }

    atom<STRING> to_atom_string(any obj) {
        return *static_cast<atom<STRING>*>(obj);
    }

    atom<STRING> to_atom_string(any_atom obj) {
        return *static_cast<atom<STRING>*>(obj);
    }
}
