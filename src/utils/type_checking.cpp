#include <lisp_types.hpp>
#include <lisp_utils.hpp>
using namespace lisp_prims;
using namespace lisp_types;

namespace lisp_utils
{
    bool is_atom(any obj) {
        return dynamic_cast<any_atom>(obj) != NULL;
    }

    bool is_list(any obj) {
        return dynamic_cast<list*>(obj) != NULL;
    }

    bool is_atom_bool(any obj) {
        return dynamic_cast<atom<BOOL>*>(obj) != NULL;
    }

    bool is_atom_int(any obj) {
        return dynamic_cast<atom<INTEGER32>*>(obj) != NULL;
    }

    bool is_atom_float(any obj) {
        return dynamic_cast<atom<FLOAT64>*>(obj) != NULL;
    }

    bool is_atom_string(any obj) {
        return dynamic_cast<atom<STRING>*>(obj) != NULL;
    }

    bool is_atom_symbol(any obj) {
        return is_atom_string(obj) && to_atom_string(obj).is_symbol();
    }

    bool is_list_nonliteral(any obj) {
        return is_list(obj) && !to_list(obj)->is_literal();
    }
}