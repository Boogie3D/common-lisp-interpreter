#include <lisp_exceptions.hpp>

namespace lisp_exceptions
{
    std::string lisp_exception::msg() const noexcept {
        return preface + std::string(runtime_error::what());
    }
}