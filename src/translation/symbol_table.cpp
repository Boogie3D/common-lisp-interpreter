#include <lisp_exceptions.hpp>
#include <lisp_types.hpp>
#include <lisp_utils.hpp>
#include <translation/symbol_table.hpp>
using namespace lisp_exceptions;
using namespace lisp_prims;
using namespace lisp_types;
using namespace lisp_utils;

namespace lisp_stack
{
    /* Begin symbol_t definitions */
    symbol_t::symbol_t() {
        is_function = true;
        func_params = nullptr;
        func_body = nullptr;
        symbol_value = nullptr;
    }

    symbol_t::symbol_t(any value) {
        symbol_value = value;
        is_function = false;
        func_params = nullptr;
        func_body = nullptr;
    }

    symbol_t::symbol_t(list* params, list* body) {
        func_params = params;
        func_body = body;
        is_function = true;
        symbol_value = nullptr;
    }
    /* End symbol_t definitions */


    /* Begin symbol_table definitions */
    bool symbol_table::empty() {
        return table.empty();
    }
    
    bool symbol_table::has_symbol(atom<STRING> symbol) {
        return (table.find(symbol.get_value()) != table.end()) ? true : false;
    }

    void symbol_table::add_symbol(atom<STRING> symbol, any value) {
        if (has_symbol(symbol))
            throw illegal_operation("symbol " + symbol.get_repr() + " already exists");

        symbol_t table_symbol(value);
        table[symbol.get_value()] = table_symbol;
    }

    void symbol_table::add_function(atom<STRING> function, list params, list body) {
        if (has_symbol(function))
            throw illegal_operation("symbol " + function.get_repr() + " already exists");

        symbol_t table_symbol(new list(params.get_value()), new list(body.get_value()));
        table[function.get_value()] = table_symbol;
    }

    void symbol_table::add_builtin_function(atom<STRING> function) {
        if (has_symbol(function))
            throw illegal_operation("symbol " + function.get_repr() + " already exists");

        symbol_t table_symbol;
        table[function.get_value()] = table_symbol;
    }

    symbol_t* symbol_table::get_symbol(atom<STRING> symbol) {
        return &table[symbol.get_value()];
    }

    void symbol_table::set_symbol(atom<STRING> symbol, any value) {
        if (!has_symbol(symbol))
            throw illegal_operation("symbol " + symbol.get_repr() + " does not exist");

        symbol_t table_symbol(value);
        table[symbol.get_value()] = table_symbol;
    }

    void symbol_table::set_function(atom<STRING> symbol, list params, list body) {
        if (!has_symbol(symbol))
            throw illegal_operation("symbol " + symbol.get_repr() + " does not exist");

        symbol_t table_symbol(new list(params.get_value()), new list(body.get_value()));
        table[symbol.get_value()] = table_symbol;
    }
    /* End symbol_table definitions */
}
