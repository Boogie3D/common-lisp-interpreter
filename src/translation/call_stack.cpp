#include <lisp_exceptions.hpp>
#include <lisp_types.hpp>
#include <lisp_utils.hpp>
#include <translation/symbol_table.hpp>
using namespace lisp_prims;
using namespace lisp_types;
using namespace lisp_utils;

namespace lisp_stack
{
    call_stack call_stack::call_stack_instance;

    call_stack& call_stack::get_instance() {
        return call_stack_instance;
    }

    void call_stack::initialize() {
        symbol_table table;

        if (!call_stack_instance.stack_frame.empty())
            throw lisp_exceptions::illegal_operation("cannot re-initialize call stack");

        // Symbols
        table.add_symbol(atom<STRING>("T"), to_any(new atom<BOOL>(BOOL::T)));
        table.add_symbol(atom<STRING>("NIL"), to_any(new atom<BOOL>(BOOL::NIL)));

        // Meta
        table.add_builtin_function(atom<STRING>("QUIT"));
        table.add_builtin_function(atom<STRING>("HELP"));

        // Constructs
        table.add_builtin_function(atom<STRING>("IF"));
        table.add_builtin_function(atom<STRING>("DEFINE"));
        table.add_builtin_function(atom<STRING>("SET!"));
        table.add_builtin_function(atom<STRING>("DEFUN"));

        // Operations
        table.add_builtin_function(atom<STRING>("+"));
        table.add_builtin_function(atom<STRING>("-"));
        table.add_builtin_function(atom<STRING>("*"));
        table.add_builtin_function(atom<STRING>("/"));

        // Functions
        table.add_builtin_function(atom<STRING>("CAR"));
        table.add_builtin_function(atom<STRING>("CDR"));
        table.add_builtin_function(atom<STRING>("CONS"));
        table.add_builtin_function(atom<STRING>("SQRT"));
        table.add_builtin_function(atom<STRING>("POW"));

        // Relations
        table.add_builtin_function(atom<STRING>(">"));
        table.add_builtin_function(atom<STRING>(">="));
        table.add_builtin_function(atom<STRING>("<"));
        table.add_builtin_function(atom<STRING>("<="));
        table.add_builtin_function(atom<STRING>("="));
        table.add_builtin_function(atom<STRING>("!="));
        table.add_builtin_function(atom<STRING>("AND"));
        table.add_builtin_function(atom<STRING>("OR"));
        table.add_builtin_function(atom<STRING>("NOT"));

        call_stack_instance.stack_frame.push_front(table);
    }

    void call_stack::push_symbol_table(symbol_table table) {
        call_stack_instance.stack_frame.push_front(table);
    }

    void call_stack::pop_symbol_table() {
        call_stack_instance.stack_frame.pop_front();
    }

    symbol_table* call_stack::peek_symbol_table() {
        return &call_stack_instance.stack_frame.front();
    }

    symbol_t* call_stack::find_symbol(atom<STRING> symbol) {
        for (symbol_table frame : call_stack_instance.stack_frame) {
            if (frame.has_symbol(symbol)) {
                return frame.get_symbol(symbol);
            }
        }

        return nullptr;
    }
}