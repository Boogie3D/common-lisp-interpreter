#include <lisp_exceptions.hpp>
#include <lisp_types.hpp>
#include <lisp_utils.hpp>
#include <translation/evaluator.hpp>
#include <translation/func_call.hpp>
#include <translation/symbol_table.hpp>
using namespace lisp_exceptions;
using namespace lisp_funcs;
using namespace lisp_prims;
using namespace lisp_types;
using namespace lisp_utils;
using namespace lisp_stack;

namespace lisp_trans
{
    any evaluate(any expr) {
        if (is_atom(expr))
            return evaluate(to_atom(expr));
        else
            return evaluate(to_list(expr));
    }

    any evaluate(any_atom expr) {
        any res;

        if (expr->is_symbol()) {
            call_stack stack = call_stack::get_instance();
            symbol_t* symbol;
            if ((symbol = stack.find_symbol(to_atom_string(expr))) != nullptr) {
                res = symbol->symbol_value;
            } else {
                throw invalid_variable(expr);
            }
        } else {
            res = to_any(expr);
        }

        return res;
    }

    any evaluate(list* expr) {
        // Empty list
        if (expr->length() == 0)
            return to_any(expr);

        // List literal
        if (expr->is_literal())
            return to_any(expr);

        if (!is_atom_string(expr->front()))
            throw illegal_operation(expr->front()->get_repr() + " is not a function");

        atom<STRING> first = to_atom_string(expr->front());

        if (!first.is_symbol())
            throw illegal_operation("'" + first.get_repr() + " is not a symbol");

        // List non-literal (function call)
        // First parameter is a symbol, check if function
        call_stack stack = call_stack::get_instance();
        symbol_t* symbol;

        if ((symbol = stack.find_symbol(first)) != nullptr) {
            if (!symbol->is_function)
                throw invalid_function(&first);

            if (symbol->func_body == nullptr)
                return call_builtin_func(expr);
            else
                return call_user_func(expr);
        } else if (is_compressed_c_r(first.get_repr())) {
            return call_builtin_func(expr);
        } else {
            throw invalid_function(&first);
        }
    }
}
