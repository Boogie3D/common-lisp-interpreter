#include <lisp_exceptions.hpp>
#include <lisp_types.hpp>
#include <std_env.hpp>
#include <translation/reader.hpp>
#include <cctype>
#include <list>
#include <sstream>
#include <stack>
using namespace lisp_exceptions;
using namespace lisp_prims;
using namespace lisp_types;

namespace lisp_trans
{
    any read_expr(std::string input, bool is_literal) {
        auto it = input.begin();
        std::stringbuf sbuf;
        int parens = 0;

        // Reading a string
        if (*it == '\"') {
            if (++it == input.end())
                throw parsing_error("'\"' is not a valid expression");

            while (it + 1 != input.end() && *it != '\"') {
                sbuf.sputc(toupper(*it));
                it++;
            }

            if (*it != '\"')
                throw parsing_error("incomplete string: " + input);

            return read_atom(sbuf.str(), false);
        }

        if (*it == '\'' && !is_literal) {
            is_literal = true;
            it++;
        }
        
        if (it == input.end())
            throw parsing_error("''' is not a valid expression");

        if (*it == '(') {
            // Reading a list
            parens++;
            it++;
            if (it == input.end())
                throw parsing_error("'" + input + "'" + " is not a valid list");

            while (it + 1 != input.end()) {
                if (*it == '(')
                    parens++;
                else if (*it == ')')
                    parens--;
                if (parens < 0)
                    throw parsing_error("'" + input + "'" + " has unbalanced parantheses");

                sbuf.sputc(toupper(*it));
                it++;
            }

            if (*it != ')')
                throw parsing_error("'" + input + "'" + " has unbalanced parantheses");
            else
                parens--;

            if (parens != 0)
                throw parsing_error("'" + input + "'" + " has unbalanced parantheses");

            return read_list(sbuf.str(), is_literal);
        } else {
            // Reading an atom
            while (it != input.end()) {
                if (isspace(*it))
                    throw parsing_error("'" + input + "'" + " is not a valid atom");

                sbuf.sputc(toupper(*it));
                it++;
            }

            return read_atom(sbuf.str(), !is_literal);
        }
    }

    any_atom read_atom(std::string input_atom, bool is_symbol) {
        INTEGER32 i;
        FLOAT64 f;
        int period_cnt = 0;
        std::stringstream s(input_atom);

        if (input_atom.empty())
            return new atom<STRING>("\"\"");

        for (char c : input_atom) {
            if (c == '.') {
                period_cnt++;
            }

            if (isalpha(c) || period_cnt > 1 || (ispunct(c) && c != '.')) {
                return new atom<STRING>(input_atom, is_symbol);
            }
        }

        if (period_cnt == 1) {
            s >> f;
            return new atom<FLOAT64>(f);
        } else {
            s >> i;
            return new atom<INTEGER32>(i);
        }
    }

    list* read_list(std::string input_list, bool is_literal) {
        std::list<any> l;
        int parens = 0;

        for (auto it = input_list.begin(); it != input_list.end(); it++) {
            std::stringbuf sbuf;
            while (it != input_list.end() && !isspace(*it)) {
                if (*it == '(') {
                    parens++;
                    sbuf.sputc(*it);
                    it++;
                    while (it != input_list.end() && parens > 0) {
                        if (*it == '(')
                            parens++;
                        else if (*it == ')')
                            parens--;

                        if (parens < 0)
                            throw parsing_error("'" + input_list + "'" + " has unbalanced parantheses");

                        sbuf.sputc(*it);
                        it++;
                    }
                } else {
                    sbuf.sputc(*it);
                    it++;
                }
            }

            if (!sbuf.str().empty())
                l.push_back(read_expr(sbuf.str(), is_literal));

            if (it == input_list.end()) {
                it--;
            }
        }

        return new list(l, is_literal);
    }
}