#include <interactive.hpp>
#include <translation/symbol_table.hpp>

int main(int argc, char *argv[]) {
    lisp_stack::call_stack::initialize();
    return lisp_interactive::repl(argc, argv);
}