#include <interactive.hpp>
#include <lisp_types.hpp>
#include <cstdio>
#include <iostream>
#include <fstream>
using namespace lisp_types;
using namespace std;

namespace lisp_interactive
{
    prompt::prompt(void)
    {
        prompt_pattern = "[%d]> ";
        counter = 1;
    }

    void prompt::print_intro() {
        ofstream session_log;
        session_log.open("lispint_session.log", ios::trunc);
        session_log << "*** BEGIN SESSION ***" << endl;
        session_log.close();

        string intro_msg = R"ascii(
   a8                                                                    8a     
  d8'  88           88                          88                       `8b    
 d8'   88           ""                          88                ,d      `8b   
d8'    88                                       88                88       `8b  
88     88           88  ,adPPYba,  8b,dPPYba,   88  8b,dPPYba,  MM88MMM     88  
88     88           88  I8[    ""  88P'    "8a  88  88P'   `"8a   88        88  
Y8,    88           88   `"Y8ba,   88       d8  88  88       88   88       ,8P  
 Y8,   88           88  aa    ]8I  88b,   ,a8"  88  88       88   88,     ,8P   
  Y8,  88888888888  88  `"YbbdP"'  88`YbbdP"'   88  88       88   "Y888  ,8P    
   "8                              88                                    8"     
                                   88                                           

Welcome to LispInt v0.1 (2019-05-01) <https://gitlab.com/Boogie3D/>

Steven Anaya 2019.

Type (help) for help.
)ascii";

        cout << intro_msg << endl;
    }

    void prompt::print_prompt() {
        ::printf(prompt_pattern, counter);
        counter++;
    }

    bool prompt::print_output(any expr) {
        ofstream session_log;
        session_log.open("lispint_session.log", ios::app);

        if (expr->get_repr() == "(HELP)") {
            print_help();
            session_log << "Output: *** HELP MESSAGE PRINTED ***" << endl << endl;
            session_log.close();
            return false;
        } else if (expr->get_repr() != "(QUIT)") {
            cout << expr->get_repr() << endl;
            session_log << "Output: " << expr->get_repr() << endl << endl;
            session_log.close();
            return false;
        }

        session_log << "Output: Bye." << endl << "*** END SESSION ***" << endl;
        return true;
    }

    void prompt::print_help() {
        string help_message = R"help(
You are in the top-level Read-Eval-Print loop (REPL).

Available commands:
    (define var exp) = evaluate exp and assign to a new variable
    (set! var exp) = evaluate exp and assign to existing variable
    (defun func (arg1 arg2 ...) exp) = define a new function
    (if test conseq alt) = if test is true then evaluate conseq, else alt
    (help) = print this help message
    (quit) = leave LispInt

Available operators: +, -, *, /

Available functions: car, cdr, sqrt, pow

Available relational operators: >, >=, <, <=, =, !=, and, or, not
)help";

        cout << help_message << endl;
    }

    void prompt::print_error(string error_msg) {
        ofstream session_log;
        session_log.open("lispint_session.log", ios::app);
        session_log << "Output: " << error_msg << endl << endl;
        cerr << error_msg << endl;
    }

    void prompt::quit_prompt() {
        cout << "Bye." << endl;
    }

    string prompt::read_input() {
        string input;

        if (!getline(cin, input)) {
            // EOF was sent, quit interpreter
            if (cin.eof()) {
                cout << endl;
                input = "(quit)";
            } else {
                exit(EXIT_FAILURE);
            }
        }

        ofstream session_log;
        session_log.open("lispint_session.log", ios::app);
        session_log << "Input: " << input << endl;
        session_log.close();

        return input;
    }
}