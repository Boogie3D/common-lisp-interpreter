#include <interactive.hpp>
#include <lisp_exceptions.hpp>
#include <lisp_types.hpp>
#include <translation/evaluator.hpp>
#include <translation/reader.hpp>
#include <stdexcept>
#include <iostream>
using namespace lisp_exceptions;
using namespace lisp_trans;
using namespace lisp_types;

namespace lisp_interactive
{
    int repl(int argc, char *argv[]) {
        prompt lisp_prompt;
        std::string response;
        any expr;
        bool quit = false;

        lisp_prompt.print_intro();
        while (quit != true) {
            lisp_prompt.print_prompt();
            try {
                response = lisp_prompt.read_input();
                expr = read_expr(response);
                expr = evaluate(expr);
                quit = lisp_prompt.print_output(expr);
            }
            catch (lisp_exception& e) {
                lisp_prompt.print_error(e.msg());
            }
        }
        lisp_prompt.quit_prompt();
        return 0;
    }
}
